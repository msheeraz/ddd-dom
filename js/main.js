// template container selectors
var news_details = document.getElementById("news_details");
var news_details_wrapper = document.getElementById("news_details_wrapper");
var focusAreaNews = document.getElementById("focusAreaNews");
var followingNews = document.getElementById("followingNews");
var marqueeText = document.getElementById("marqueeText");


// get the navigation items listed in nav
var navigationItems = document.getElementsByClassName("navigator");
// initlize a loop to add click event listener
for (var i = 0; i < navigationItems.length; i++) {
  navigationItems[i].addEventListener(
    "click",
    function (e) {
      // prevent muldiple submissions
      e.preventDefault();
      // get the list item clicked
      var li = e.srcElement.closest(".navigator");
      // get the attribute value of navigator that has been clicked
      var navigator = e.srcElement.getAttribute("navigator");
      // convert to lowercase
      navigator = navigator.toLowerCase();
      // check if thet is a value in navigator
      if (navigator !== null) {
        for (var j = 0; j < navigationItems.length; j++) {
          // remove active link class
          navigationItems[j].classList.remove("active");
        }
        // add active link class
        li.classList.add("active");
        // finally pass the active navigator to json page builder class
        jsonPageBuilder(navigator);
      }
    },
    false
  );
}

// template components
/**
 * Generate main flashing news articles
 * 
 * @params img,title,date,brief,is_breaking,is_developing, full_content, author, copyright 
 */
function flashing_news_main_component(
  img,
  title,
  date,
  brief,
  is_breaking,
  is_developing,
  full_content,
  author,
  copyright
) {
  // checks if is breaking flag is true, if true/false then assignes respective text with effects
  if (is_breaking) {
    is_breaking = "<div class='breakingflag blink_me'>BREAKING NEWS</div>";
  } else {
    is_breaking = "";
  }
  // checks if is developing flag is true, if true/false then assignes respective text with effects
  if (is_developing) {
    is_developing =
      "<div class='developingflag blink_me'>DEVELOPING NEWS</div>";
  } else {
    is_developing = "";
  }

  // final return of the template with passed parameters embedded.
  return (
    "<div full_content = " + JSON.stringify(full_content) + " author=" + JSON.stringify(author.name) + " author_desc=" + JSON.stringify(author.desc) + " author_img=" + JSON.stringify(author.img) + " img=" + JSON.stringify(img) + " title=" + JSON.stringify(title) + " date=" + JSON.stringify(date) + "  is_breaking=" + JSON.stringify(is_breaking) + "   is_developing =" + JSON.stringify(is_developing) + " copyright =" + JSON.stringify(copyright) + " onclick =displayDetails(this) >" +
    '<div  class="col-6 col-s-12"><div class="col-12 col-s-12 flashing_news_main" style="background-repeat: no-repeat;background-size: cover;background-image: linear-gradient(rgba(0,0,0,0.0),rgba(0,0,0,0.8)), url(' +
    img +
    ')"> <div class="row"> <div class="col-12 col-s-12"> <h1>' +
    title +
    "</h1> <p> " +
    brief +
    ' </p> <span> <img src="img/clock.png" class="news_clock" alt="clock" /> <p>' +
    date +
    "</p> </span> </div> </div> " +
    is_developing +
    is_breaking +
    "</div></div></div>"
  );
}

// following news
/**
 * Generate main following news articles on bottom
 * 
 * @params img, title, date, brief_content, is_breaking, is_developing, full_content, author, copyright
 */
function following_news(img, title, date, brief_content, is_breaking, is_developing, full_content, author, copyright) {
  // checks if is breaking flag is true, if true/false then assignes respective text with effects
  if (is_breaking) {
    is_breaking = "<div class='breakingflagsmall blink_me'>BREAKING NEWS</div>";
  } else {
    is_breaking = "";
  }
  // checks if is developing flag is true, if true/false then assignes respective text with effects
  if (is_developing) {
    is_developing =
      "<div class='developingflagsmall blink_me'>DEVELOPING NEWS</div>";
  } else {
    is_developing = "";
  }
  // final return of the template with passed parameters embedded.
  return (
    '<article>' +
    "<div style='cursor:pointer' full_content = " + JSON.stringify(full_content) + " author=" + JSON.stringify(author.name) + " author_desc=" + JSON.stringify(author.desc) + " author_img=" + JSON.stringify(author.img) + " img=" + JSON.stringify(img) + " title=" + JSON.stringify(title) + " date=" + JSON.stringify(date) + "  is_breaking=" + JSON.stringify(is_breaking) + "   is_developing =" + JSON.stringify(is_developing) + " copyright =" + JSON.stringify(copyright) + " onclick =displayDetails(this) >" +
    '<div class="col-12 col-s-12" style=" height:250px; background-repeat: no-repeat;background-size: cover;background-image: linear-gradient(rgba(0,0,0,0.0),rgba(0,0,0,0.8)), url(' +
    img +
    ')" > <div class="col-12 col-s-12"> <h3>' +
    title +
    "</h3> </div> </div> </div></article>"
  );
}

// news details
/**
 * Generate news details from props of the function bound element
 * 
 * @params info
 */
function displayDetails(info) {
  // on start, make the focus area and following news area hidden on screen
  focusAreaNews.style.display = "none";
  followingNews.style.display = "none";
  // checks if is breaking flag is true, if true/false then assignes respective text with effects
  if (info.getAttribute("is_breaking")) {
    is_breaking = "<div class='breakingflagsmall blink_me'>BREAKING NEWS</div>";
  } else {
    is_breaking = "";
  }
  // checks if is developing flag is true, if true/false then assignes respective text with effects
  if (info.getAttribute("is_developing")) {
    is_developing =
      "<div class='developingflagsmall blink_me'>DEVELOPING NEWS</div>";
  } else {
    is_developing = "";
  }
  // start building the template
  // start with empty variable
  var newsDetails = "";
  // concatinate date into variable
  newsDetails += "<div class='news_details_wrapper'>";
  newsDetails += "<div class='heading'>" + info.getAttribute("title") + "</div>";
  newsDetails += "<div class='author_wrapper'><div class='date_wrapper'>" + info.getAttribute("date") + "</div><div class='author_img' align='center'><img src='" + info.getAttribute("author_img") + "' /></div><div class='author'>" + info.getAttribute("author") + ", " + info.getAttribute("author_desc") + "</div></div>";
  newsDetails += is_breaking + is_developing + "<div class='img_wrapper'><img src='" + info.getAttribute("img") + "' /></div>";
  newsDetails += "<p> Copyright " + info.getAttribute("copyright") + "</p>";
  newsDetails += "<div class='details'>" + info.getAttribute("full_content") + "</div>";
  // once done with template build, embed into html element
  news_details_wrapper.innerHTML = newsDetails;
  // finally show html element to the user
  news_details.style.display = "block";

}
/**
 * Generate close news details modal
 * 
 * @params nil
 */
function closeDetails() {
  // show focus news area 
  focusAreaNews.style.display = "block";
  // show following news area 
  followingNews.style.display = "block";
  // hide news details area
  news_details.style.display = "none";
}

// init
// call json page builder method to load the data on page load
jsonPageBuilder("");

/**
 * Generate data and call individual methods with data
 * 
 * @params navigator
 */

function jsonPageBuilder(navigator) {

  // check if the navigator parameter is empty or not, if empty, make default to home navigator
  if (navigator == "" || navigator == null) {
    navigator = "home";
  }


  // call to a custom method to load json data as response
  loadJSON(function (response) {
    // declare variables and assign response return from arbitary function call with JSON parsing
    var json_Obj = JSON.parse(response);
    // declare and assign new variable with json object and reach main array element
    var articles = json_Obj.articles;
    // set default components empty
    focusAreaNews.innerHTML = null;
    followingNews.innerHTML = null;
    // set default variable empty
    var focusAreaNewsData = "";
    var followingNewsData = "";
    // news data counter value: seperate number of records to be displayed as a control
    var newsDataCounter = 0;

    // intialize a for loop starting from 0 till end of article length with 1 incremental value
    for (var i = 0; i < articles.length; i++) {
      // declare and assign main array element with loop children values 
      var article = articles[i];
      // check if nvaigation value meets the currently active navigation value. This operation basically filters for navigation pages; filter relavent data from json data stream to 
      // set for the navigation.
      if (article.type == navigator || navigator == "home") {
        // increment news data counter value; this will help to seperate number of records to be displayed as a control
        newsDataCounter++
        // get main focus area news controlled by news data counter value, extracting 2 records
        if (newsDataCounter < 3) {
          focusAreaNewsData += flashing_news_main_component(
            article.img,
            article.title,
            article.date,
            article.brief_content,
            article.is_breaking,
            article.is_developing,
            article.full_content,
            article.author,
            article.copyright
          );
        }
        // get following news controlled by news data counter value, extracting rest of the records  records
        if (newsDataCounter >= 3) {
          followingNewsData += following_news(
            article.img,
            article.title,
            article.date,
            article.brief_content,
            article.is_breaking,
            article.is_developing,
            article.full_content,
            article.author,
            article.copyright
          );
        }
      }

    }

    // fill and display the data in respective containers
    focusAreaNews.innerHTML = focusAreaNewsData;
    followingNews.innerHTML = followingNewsData;

    // extract and display marquee text
    // intialize a for loop starting from 0 till end of articles
    for (var i = 0; i < articles.length; i++) {
      var article = articles[i];
      // check if the marquee is not null or empty
      if (article.marquee_text != "" || article.marquee_text != null) {
        // concatnate marquee text to marquee component
        marqueeText.innerHTML += article.marquee_text + " | ";
      }
    }
  });
}

/**
 * function to read and return local json file
 * 
 * @params callback
 */
function loadJSON(callback) {
  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open("GET", "data/data.json", true);
  xobj.onreadystatechange = function () {
    if (xobj.readyState == 4 && xobj.status == "200") {
      callback(xobj.responseText);
    }
  };
  xobj.send(null);
}

